# irz-rxx-toolchain
This toolchain is specifically targets RUH line of industrial mobile broadband routers.

## Build HOTWO
First, you need to install development libraries and utilities:

```
#!bash

sudo apt-get install autoconf automake libtool libexpat1-dev libncurses5-dev bison flex patch curl cvs texinfo build-essential subversion gawk python-dev gperf git
```

Also, you will need to install [crosstool-ng][1].

Clone repository and fetch submodules:

```
#!bash

git clone https://bitbucket.org/radiofid/irz-rxx-toolchain.git
cd irz-rxx-toolchain
git submodule init
git submodule update
```

And compile.

```
#!bash

ct-ng build
```

Compilation will usually take anywhere from a ten minutes to an hour or two depending on your hardware. 
After compilation is finished toolchain will be installed into `~/local/arm-irz-linux-uclibcgnueabi`

## Prebuilt toolchain
As an alternative option you can always download latest prebuilt toolchain:

[arm-irz-linux-uclibcgnueabi.tar.xz][2] - i686, statically linked.

## Project examples
Look into `examples` subdirectory for the example toolchain usage.


[1]: http://crosstool-ng.org/
[2]: https://bitbucket.org/radiofid/irz-rxx-toolchain/downloads/arm-irz-linux-uclibcgnueabi.tar.xz
