set(CMAKE_SYSTEM_NAME Linux)

# specify the cross compiler
set(CMAKE_C_COMPILER arm-irz-linux-uclibcgnueabi-gcc)

#set(CMAKE_INSTALL_SO_NO_EXE 0)
#set(CMAKE_PROGRAM_PATH "/home/zs/work/router-irz-wimax-fw/output/host/usr/bin")
#set(CMAKE_FIND_ROOT_PATH "/home/zs/work/router-irz-wimax-fw/output/host/usr/arm-buildroot-linux-uclibcgnueabi/sysroot")
#set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
#set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
#set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
